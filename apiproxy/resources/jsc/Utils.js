var bodyContent = context.getVariable('request.content');
var regex = new RegExp(/^[a-zA-Z0-9+]+([\._-][a-zA-Z0-9]+)*@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9]+){0,4}\.[a-zA-Z0-9]{1,4}$/);

var timeStamp = new Date().toISOString().replace('Z', '');
context.setVariable("timeStamp", timeStamp);

var body = JSON.parse(bodyContent);
var email = body.email;
context.setVariable("isValidEmail", "true");    
if(typeof email != 'undefined' && !regex.test(email)) {
    context.setVariable("isValidEmail", "false");
    context.setVariable("errorMessage", "Please provide the valid email");
    context.setVariable("path", "/properties/email/invalid");
}

if (typeof body.phones != 'undefined') {
    var newbody = body;
    for ( var phone = 0; phone < body.phones.length; phone++) {
        if (body.phones[phone].countryCode === null) {
            newbody.phones[phone].countryCode = "1";
        }
    }
    request.content = JSON.stringify(newbody);
}

context.setVariable("correlationId",body.correlationId);
